//
//  ExchangeService.m
//  MSExchange
//
//  Created by Marco Santarossa on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "ExchangeService.h"

@implementation ExchangeService

+(float)exchangeValue:(float)value fromRate:(float)firstRate toRate:(float)secondRate {
    return (value / firstRate) * secondRate;
}

@end
