//
//  CurrencyParserService.m
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "CurrencyParserService.h"

@interface CurrencyParserService()<NSXMLParserDelegate>

@property(nonatomic, retain) NSMutableDictionary* currencyParsed;
@property(nonatomic, retain) NSXMLParser* xmlParser;

@end

@implementation CurrencyParserService

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currencyParsed = [[NSMutableDictionary alloc]init];
    }
    return self;
}

-(void)startParsing {
    // I should add this URL in a config file.
    NSString* url = @"https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    self.xmlParser = [[NSXMLParser alloc]initWithContentsOfURL:[NSURL URLWithString:url]];
    self.xmlParser.delegate = self;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.xmlParser parse];
    });
}

#pragma mark - NSXMLParserDelegate

-(void)parserDidStartDocument:(NSXMLParser *)parser {
    [self.currencyParsed removeAllObjects];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    NSString* mainNode = @"Cube";
    NSString* currencyNode = @"currency";
    NSString* rateNode = @"rate";
    if (![elementName isEqualToString:mainNode] || [attributeDict objectForKey:currencyNode] == NULL || [attributeDict objectForKey:rateNode] == NULL) {
        return;
    }
    
    NSNumber* rate = [NSNumber numberWithFloat:[[attributeDict objectForKey:rateNode]floatValue]];
    NSString* currencyName = [NSString stringWithString:[attributeDict objectForKey:currencyNode]];
    [self.currencyParsed setObject:rate forKey:currencyName];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate currenciesDidFetch:self.currencyParsed];
    });
}


@end
