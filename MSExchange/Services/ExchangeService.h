//
//  ExchangeService.h
//  MSExchange
//
//  Created by Marco Santarossa on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExchangeService : NSObject

+(float)exchangeValue:(float)value fromRate:(float)firstRate toRate:(float)secondRate;

@end
