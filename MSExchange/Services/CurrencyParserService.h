//
//  CurrencyParserService.h
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CurrencyParserServiceDelegate <NSObject>

-(void)currenciesDidFetch:(NSDictionary*)currencies;

@end

@interface CurrencyParserService : NSObject

@property(nonatomic, weak) id<CurrencyParserServiceDelegate> delegate;

-(void)startParsing;

@end
