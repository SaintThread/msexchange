//
//  CurrencyPagingWireframe.h
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrencyInputWireframe.h"

@class CurrencyPagingWireframe;
@class Currency;

@protocol CurrencyPagingWireframeDelegate <NSObject>

-(void)pageDidChange:(CurrencyPagingWireframe*)sender pageIndex:(NSInteger)index;
-(void)valueToExchangeDidChange:(CurrencyPagingWireframe*)sender value:(float)value;

@end

@interface CurrencyPagingWireframe : NSObject<CurrencyInputWireframeDelegate>

@property(nonatomic, weak) id<CurrencyPagingWireframeDelegate> delegate;

-(void)presentInParent:(UIViewController*)parentViewController view:(UIView*)view startIndex:(NSInteger)index;

-(void)updateCurrencies:(NSArray<Currency*>*)currencies;
-(void)updateCurrentValueToExchange:(float)value;

-(Currency*)currentCurrency;
-(float)valueToExchange;

@end
