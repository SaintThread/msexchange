//
//  CurrencyPagingWireframe.m
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "CurrencyPagingWireframe.h"
#import "CurrencyInputViewController.h"
#import "Currency.h"

@interface CurrencyPagingWireframe()<UIPageViewControllerDataSource, UIPageViewControllerDelegate>


@property(nonatomic, strong) NSMutableArray<CurrencyInputWireframe *>* currencyInputWireframes;
@property(nonatomic, strong) UIPageViewController* viewController;

@property(nonatomic, assign) NSInteger currentPage;

@end

@implementation CurrencyPagingWireframe

-(void)presentInParent:(UIViewController*)parentViewController view:(UIView*)view startIndex:(NSInteger)index {
    self.currentPage = index;
    
    self.viewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                                 navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                               options:NULL];

    [self addPagingIn:parentViewController view:view];

    self.viewController.dataSource = self;
    self.viewController.delegate = self;
}

-(void) addPagingIn:(UIViewController*)parentViewController view:(UIView*)view {
    [parentViewController addChildViewController:self.viewController];

    UIView* subview = self.viewController.view;
    subview.translatesAutoresizingMaskIntoConstraints = false;

    [view addSubview:subview];

    NSDictionary* views = [[NSDictionary alloc]initWithObjectsAndKeys:subview, @"subview", nil];
    NSArray* verticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subview]|"
                                                                          options:0
                                                                          metrics:NULL
                                                                            views:views];

    NSArray* horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subview]|"
                                                                            options:0
                                                                            metrics:NULL
                                                                              views:views];
    [NSLayoutConstraint activateConstraints:verticalConstraint];
    [NSLayoutConstraint activateConstraints:horizontalConstraint];

    [self.viewController didMoveToParentViewController:parentViewController];
}

-(void)updateCurrencies:(NSArray<Currency*>*)currencies {
    [self addCurrencyInputWireframesWithCurrencies:currencies];
    [self showPage:self.currentPage];
}

-(void)showPage:(NSInteger)index {
    // I don't check if index is less than my array count so I can debug with a crash the code.
    // In production, I would add the check and remove the assert.
    assert(index < self.currencyInputWireframes.count);

    self.currentPage = index;

    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:index];
    [self.viewController setViewControllers:@[wireframe.viewController]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:false
                                 completion:NULL];
}

-(void)updateCurrentValueToExchange:(float)value {
    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:self.currentPage];
    [wireframe updateValueToExchange:value];
}

-(void)addCurrencyInputWireframesWithCurrencies:(NSArray<Currency*>*)currencies {
    self.currencyInputWireframes = [[NSMutableArray alloc]init];
    for (NSInteger i = 0; i != currencies.count; i++) {
        Currency* currency = [currencies objectAtIndex:i];
        CurrencyInputWireframe* wireframe = [[CurrencyInputWireframe alloc]initWithIndex:i currency:currency];
        wireframe.delegate = self;
        [self.currencyInputWireframes addObject:wireframe];
    }
}

-(Currency*)currentCurrency {
    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:self.currentPage];
    return wireframe.currency;
}

-(float)valueToExchange {
    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:self.currentPage];
    return [wireframe valueToExchange];
}

#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (!completed) {
        return;
    }
    
    UIViewController* currentViewController = [self.viewController.viewControllers objectAtIndex:0];
    NSInteger index = currentViewController.view.tag;
    self.currentPage = index;
    
    [self.delegate pageDidChange:self pageIndex:index];
}

#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSInteger index = viewController.view.tag - 1;
    if (index < 0) {
       index = self.currencyInputWireframes.count - 1;
    }
    
    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:index];
    return wireframe.viewController;
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSInteger index = viewController.view.tag + 1;
    if (index >= self.currencyInputWireframes.count) {
        index = 0;
    }
    
    CurrencyInputWireframe* wireframe = [self.currencyInputWireframes objectAtIndex:index];
    return wireframe.viewController;
}

-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return self.currencyInputWireframes.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return self.currentPage;
}

#pragma mark - CurrencyInputWireframeDelegate
-(void)valueToExchangeDidChange:(float)value {
    [self.delegate valueToExchangeDidChange:self value:value];
}

@end
