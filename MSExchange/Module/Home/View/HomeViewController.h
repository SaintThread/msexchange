//
//  HomeViewController.h
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property (weak, nonatomic, readonly) IBOutlet UIView *topViewContainer;
@property (weak, nonatomic, readonly) IBOutlet UIView *bottomViewContainer;

@end
