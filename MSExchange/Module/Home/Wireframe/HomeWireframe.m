//
//  HomeWireframe.m
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "HomeWireframe.h"
#import "HomeViewController.h"
#import "CurrencyPagingWireframe.h"
#import "HomeInteractor.h"
#import "ExchangeService.h"
#import "Currency.h"

@interface HomeWireframe()<CurrencyPagingWireframeDelegate, HomeInteractorDelegate>

@property(nonatomic, retain) HomeViewController* viewController;

@property(nonatomic, retain) CurrencyPagingWireframe* topCurrencyPagingWireframe;
@property(nonatomic, retain) CurrencyPagingWireframe* bottomCurrencyPagingWireframe;

@property(nonatomic, retain) HomeInteractor* interactor;

@end

@implementation HomeWireframe

-(void)presentInParent:(UINavigationController*)parentViewController {
    self.viewController = [[HomeViewController alloc] init];
    [parentViewController presentViewController:self.viewController animated:false completion:NULL];
    
    self.interactor = [[HomeInteractor alloc]init];
    self.interactor.delegate = self;
    [self.interactor startParsingPolling];
}

#pragma mark - HomeInteractorDelegate
-(void)newCurrenciesAvailable:(NSArray<Currency *>*)currencies {
    [self addTopPagingIfNeeded];
    [self addBottomPagingIfNeeded];

    [self.topCurrencyPagingWireframe updateCurrencies:currencies];
    [self.bottomCurrencyPagingWireframe updateCurrencies:currencies];
}

-(void)addBottomPagingIfNeeded {
    if (self.bottomCurrencyPagingWireframe != NULL) {
        return;
    }
    self.bottomCurrencyPagingWireframe = [[CurrencyPagingWireframe alloc]init];
    self.bottomCurrencyPagingWireframe.delegate = self;
    [self.bottomCurrencyPagingWireframe presentInParent:self.viewController view:self.viewController.bottomViewContainer startIndex:1];
}

-(void)addTopPagingIfNeeded {
    if (self.topCurrencyPagingWireframe != NULL) {
        return;
    }
    self.topCurrencyPagingWireframe = [[CurrencyPagingWireframe alloc]init];
    self.topCurrencyPagingWireframe.delegate = self;
    [self.topCurrencyPagingWireframe presentInParent:self.viewController view:self.viewController.topViewContainer startIndex:0];
}

#pragma mark - CurrencyPagingWireframeDelegate
-(void)pageDidChange:(CurrencyPagingWireframe *)sender pageIndex:(NSInteger)index {
    CurrencyPagingWireframe* oppositePaging = sender == self.topCurrencyPagingWireframe ? self.bottomCurrencyPagingWireframe : self.topCurrencyPagingWireframe;

    float exchangeValue = [oppositePaging valueToExchange];
    float senderExchangeValue = [ExchangeService exchangeValue:exchangeValue fromRate:oppositePaging.currentCurrency.rate toRate:sender.currentCurrency.rate];
    [sender updateCurrentValueToExchange:senderExchangeValue];
}

-(void)valueToExchangeDidChange:(CurrencyPagingWireframe*)sender value:(float)value {
    CurrencyPagingWireframe* oppositePaging = sender == self.topCurrencyPagingWireframe ? self.bottomCurrencyPagingWireframe : self.topCurrencyPagingWireframe;

    float oppositeExchangeValue = [ExchangeService exchangeValue:value fromRate:sender.currentCurrency.rate toRate:oppositePaging.currentCurrency.rate];
    [oppositePaging updateCurrentValueToExchange:oppositeExchangeValue];
}

@end
