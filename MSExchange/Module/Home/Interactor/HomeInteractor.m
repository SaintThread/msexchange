//
//  HomeInteractor.m
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "HomeInteractor.h"
#import "CurrencyParserService.h"
#import "Currency.h"

@interface HomeInteractor()<CurrencyParserServiceDelegate>

@property(nonatomic, retain) NSArray* currencyNames;
@property(nonatomic, retain) CurrencyParserService* parser;
@property(nonatomic, retain) NSTimer* timer;
@property(nonatomic, assign) NSTimeInterval timeInterval;

@end

@implementation HomeInteractor

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currencyNames = [NSArray arrayWithObjects:@"GBP", @"USD", @"EUR", nil];
        self.parser = [[CurrencyParserService alloc]init];
        self.parser.delegate = self;
        self.timeInterval = 30.0;
    }
    return self;
}

-(void)dealloc {
    [self.timer invalidate];
    self.timer = nil;
}

-(void)startParsingPolling {
    [self.parser startParsing];
}

#pragma mark - CurrencyParserServiceDelegate
-(void)currenciesDidFetch:(NSDictionary*)currencies {
    NSMutableArray<Currency*>* tempCurrencies = [[NSMutableArray alloc]init];
    for (NSString* key in [currencies allKeys]) {
        for (NSString* name in self.currencyNames) {
            if ([key isEqualToString:name]) {
                float rate = [[currencies objectForKey:key]floatValue];
                Currency* currency = [self currencyWithName:key rate:rate];
                [tempCurrencies addObject:currency];
            }
        }
    }
    Currency* currency = [self currencyWithName:@"EUR" rate:1.0];
    [tempCurrencies addObject:currency];

    [self.delegate newCurrenciesAvailable:[NSArray arrayWithArray:tempCurrencies]];

    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeInterval
                                     target:self
                                   selector:@selector(startParsingPolling)
                                   userInfo:nil
                                    repeats:NO];
}

-(Currency*)currencyWithName:(NSString*)name rate:(float)rate {
    Currency* currency = [[Currency alloc]init];
    currency.name = name;
    currency.rate = rate;
    return currency;
}

@end
