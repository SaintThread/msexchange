//
//  HomeInteractor.h
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Currency;

@protocol HomeInteractorDelegate <NSObject>

-(void)newCurrenciesAvailable:(NSArray<Currency *>*)currencies;

@end

@interface HomeInteractor : NSObject

@property(nonatomic, readonly) NSArray* currencyNames;
@property(nonatomic, weak) id<HomeInteractorDelegate> delegate;

-(void)startParsingPolling;

@end
