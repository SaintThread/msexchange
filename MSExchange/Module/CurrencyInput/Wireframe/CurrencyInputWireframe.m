//
//  CurrencyInputWireframe.m
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "CurrencyInputWireframe.h"
#import "CurrencyInputViewController.h"
#import "Currency.h"
#import "CurrencyInputPresenter.h"

@interface CurrencyInputWireframe()

@property(nonatomic, strong) CurrencyInputViewController* viewController;
@property(nonatomic, strong) CurrencyInputPresenter* presenter;
@property(nonatomic, strong) Currency* currency;

@end

@implementation CurrencyInputWireframe

-(instancetype)initWithIndex:(NSInteger)index currency:(Currency*)currency {
    self = [super init];
    if (self) {
        self.currency = currency;
        
        self.presenter = [[CurrencyInputPresenter alloc]initWithCurrency:currency];
        self.viewController = [[CurrencyInputViewController alloc]init];
        self.viewController.presenter = self.presenter;
        self.presenter.delegate = self.viewController;
        self.viewController.view.tag = index;

        // Used to show KVO
        [self.presenter addObserver:self forKeyPath:@"valueToExchange" options:NSKeyValueObservingOptionNew context:NULL];
    }
    return self;
}

-(void)dealloc {
    [self.presenter removeObserver:self forKeyPath:@"valueToExchange"];
}

-(void)updateValueToExchange:(float)value {
    [self.presenter updateValueToExchange:value];
}

-(float)valueToExchange {
    return self.presenter.valueToExchange;
}

#pragma mark - KVO
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (![keyPath isEqualToString:@"valueToExchange"] || ![object isKindOfClass:CurrencyInputPresenter.class]) {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        return;
    }

    float newValue = [[change objectForKey:NSKeyValueChangeNewKey] floatValue];

    [self.delegate valueToExchangeDidChange:newValue];
}

@end
