//
//  CurrencyInputWireframe.h
//  MSExchange
//
//  Created by Santarossa, Marco (iOS Developer) on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Currency;
@class CurrencyInputViewController;

@protocol CurrencyInputWireframeDelegate <NSObject>

-(void)valueToExchangeDidChange:(float)value;

@end

@interface CurrencyInputWireframe : NSObject

@property(nonatomic, weak) id<CurrencyInputWireframeDelegate> delegate;

@property(nonatomic, strong, readonly) Currency* currency;
@property(nonatomic, strong, readonly) CurrencyInputViewController* viewController;

-(instancetype)initWithIndex:(NSInteger)index currency:(Currency*)currency;

-(void)updateValueToExchange:(float)value;

-(float)valueToExchange;

@end
