//
//  CurrencyInputViewController.m
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "CurrencyInputViewController.h"

@interface CurrencyInputViewController()

@property (weak, nonatomic) IBOutlet UILabel *currencyNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueToExchangeTextField;

@end

@implementation CurrencyInputViewController

-(void)viewDidLoad {
    [super viewDidLoad];

    self.currencyNameLabel.text = self.presenter.currencyName;
    self.valueToExchangeTextField.text = [NSString stringWithFormat:@"%.2f", self.presenter.valueToExchange];
}

- (IBAction)textFieldDidChange:(UITextField *)textField {
    self.presenter.valueToExchange = [self.valueToExchangeTextField.text floatValue];
}

#pragma mark - CurrencyInputPresenterDelegate
-(void)valueToExchangeDidChange:(float)value {
    self.valueToExchangeTextField.text = [NSString stringWithFormat:@"%.2f", value];
}

@end
