//
//  CurrencyInputViewController.h
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CurrencyInputPresenter.h"

@interface CurrencyInputViewController : UIViewController<CurrencyInputPresenterDelegate>

@property(nonatomic, weak) CurrencyInputPresenter* presenter;

@end
