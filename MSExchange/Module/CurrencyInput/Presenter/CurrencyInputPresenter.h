//
//  CurrencyInputPresenter.h
//  MSExchange
//
//  Created by Marco Santarossa on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Currency;

@protocol CurrencyInputPresenterDelegate <NSObject>

-(void)valueToExchangeDidChange:(float)value;

@end

@interface CurrencyInputPresenter : NSObject

@property(nonatomic, weak)id<CurrencyInputPresenterDelegate> delegate;

-(instancetype)initWithCurrency:(Currency*)currency;

@property(nonatomic, copy, readonly) NSString* currencyName;
@property(nonatomic, assign) float valueToExchange;

-(void)updateValueToExchange:(float)value;

@end
