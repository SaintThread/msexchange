//
//  CurrencyInputPresenter.m
//  MSExchange
//
//  Created by Marco Santarossa on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "CurrencyInputPresenter.h"
#import "Currency.h"

@interface CurrencyInputPresenter()

@property(nonatomic, copy) NSString* currencyName;

@end

@implementation CurrencyInputPresenter

-(instancetype)initWithCurrency:(Currency*)currency {
    self = [super init];
    if (self) {
        self.valueToExchange = 0;
        
        self.currencyName = currency.name;
    }
    return self;
}

-(void)updateValueToExchange:(float)value {
    _valueToExchange = value;

    [self.delegate valueToExchangeDidChange:value];
}

@end
