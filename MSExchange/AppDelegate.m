//
//  AppDelegate.m
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeWireframe.h"


@interface AppDelegate ()

@property(nonatomic, retain) HomeWireframe* wireframe;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] init];
    self.window.rootViewController = [[UIViewController alloc]init];
    [self.window makeKeyAndVisible];

    self.wireframe = [[HomeWireframe alloc]init];
    [self.wireframe presentInParent:self.window.rootViewController];

    return YES;
}

@end
