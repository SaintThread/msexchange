//
//  Currency.h
//  MSExchange
//
//  Created by Marco Santarossa on 06/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Currency : NSObject

@property(nonatomic, copy) NSString* name;
@property(nonatomic, assign) float rate;

@end
