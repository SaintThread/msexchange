//
//  main.m
//  MSExchange
//
//  Created by Marco Santarossa on 05/09/2017.
//  Copyright © 2017 MarcoSantaDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
