#TODO

I left some points in todo because I think I've already shown how I write code and I can explain here how I would complete these points. 

* Show active currency rate: I would have used the same approach used to keep the currency values updated. I would have used the service to provide the current currency for each input view and inject it through the wireframes.
* On “Exchange” tap perform exchange operation: A simple UIButton added below the two paging container views. I would have added a presenter to proxy the button action. Before completing the task I would have fetched the rates to be sure to have the latests.
* User has 100 units of each currency on launch of the app: I would have used a new model `Wallet` with the name of the currency and the total units available. Then I would have used an handler `WalletRepo` with the array of wallets. Finally, I would have updated the units once the user exchanges successfully.
* If the user has insufficient funds before exchange, need to inform him about that: An `UIAlertController` after checking in the `WalletRepo` object.

#Info Project

* Language: Objc. I'm not confident with this language since I stopped using it a couple of years ago. I used a lot of delegats even if I would have used some completions with closures like with Swift. Swift is currently my stronger language. I consider this ObjC code quite low quality because I should study how to perform optimization. I wrote Objc when I was a Junior.
* Architecture: VIPER. I abused of wireframe to rush the project and I didn't have the time to study a good solution. With Swift, I'm confident with any kind of architectures with/without RxSwift. I also created a new architecture: [4V Engine](https://marcosantadev.com/new-ios-software-architecture-4v-engine/).


#Notes

* The keyboard doesn't have a return and a button for decimal numbers. I would achieved the dismiss with a gesture recognizer in the view and I would added a custom button programmatically for the `.` symbol.
* I didn't pay efforts in the UI which is quite ugly.
* I didn't write tests and UI tests to rush the recruitment process. I can explain how I would test this face to face. I wrote some articles about testing to show my strong skills on Dependency Injection, Mocking, Test Doubles and so on. You can have a look here: [Testing articles](https://marcosantadev.com/tag/testing/).
* Everything not done, I'm able to explain face to face.
* I didn't check for memory leaks but I'm able to explain how to prevent it.